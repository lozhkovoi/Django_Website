from django.shortcuts import render
from django.http import *
from django.template.response import TemplateResponse
from .forms import UserForm
# Create your views here.
def index(request):
    if request.method =="POST":
        name=request.POST.get("name")
        #age=request.POST.get("age")
        return HttpResponse("<h2>Hello,{0}</h2>".format(name))
    else:    
        userform=UserForm()
        return render(request,"index.html",{"form":userform})